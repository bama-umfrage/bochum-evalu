import pandas as pd


def normalize_series(series: pd.Series) -> pd.Series:
	"""Normalize a series of values to percentage values"""
	return series * 100 / series.sum()
