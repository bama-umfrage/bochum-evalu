import warnings

import pandas as pd


def set_category_index(df: pd.DataFrame, categories: dict) -> pd.DataFrame:
	with warnings.catch_warnings(record=True):
		index = pd.DataFrame()
		index = index.append(categories, ignore_index=True).transpose()
		index.columns = ["index"]
	df = df.join(index).sort_values(by="index")
	return df.drop(columns=["index"])


# FIXME: doesn't work
def fill_empty(df: pd.DataFrame, categories: dict) -> pd.DataFrame:
	for k, v in categories.items():
		if k not in df.index:
			df[k] = 0
	return df
